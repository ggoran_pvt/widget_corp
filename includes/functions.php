<?php
/**
 * Created by PhpStorm.
 * User: Night5talker
 * Date: 15/02/2015
 * Time: 11:11
 */

function redirect_to($new_location){
    header ("Location: ".$new_location);
    exit;
}

function mysqli_prep($string){
    global $connection;
    $escaped_string = mysqli_real_escape_string($connection,$string);
    return $escaped_string;
}

function confirm_query($result_set){
    if(!$result_set){
        die("Query failed");
    }
}

function form_errors($errors = array()){
    $output = "";
    if(!empty($errors)){
        $output .= '<div class="error"> ';
        $output .= 'please fix the following errors: ';
        $output .= '<ul>';
        foreach ($errors as $key => $error) {
            $output .= '<li>'.htmlentities($error).'</li>';
        }
        $output .= '</ul>';
        $output .= '</div>';

    }
    return $output;
}

function find_all_subjects(){
    global $connection;
    //db query
    $query = "SELECT * FROM subjects ORDER BY position ASC;";
    $subject_set = mysqli_query($connection,$query);
    //check for query errors
    confirm_query($subject_set);
    return $subject_set;
}

function find_pages_for_subjects($subject_id){
    global $connection;

    $query = "SELECT * FROM pages WHERE visible = 1 AND subject_id ={$subject_id};";
    $page_set = mysqli_query($connection,$query);
    //check for query errors
    confirm_query($page_set);
    return $page_set;
}


//function gets 2 params - selected_subject_id and selected_page_id (ARRAY or NULL)
function navigation($subject_array, $page_array){
    $output = '<ul class="subjects">';
    //db query
    $subject_set = find_all_subjects();
    while($subject = mysqli_fetch_assoc($subject_set)){
        $output .= '<li ';
        if($subject_array && $subject["id"] == $subject_array["id"]) {
            $output .= ' class="selected" ';
        }
        $output .= '><a href="manage_content.php?subject='.urlencode($subject["id"]).'"> ';
        $output .= htmlentities($subject["menu_name"]);
        $output .= '</a>';
        //db query
        $page_set = find_pages_for_subjects($subject["id"]);
        $output .= '<ul class="pages">';
        while($page = mysqli_fetch_assoc($page_set)){
            $output .= '<li';
            if($page_array && $page["id"]==$page_array["id"]){
                $output .= ' class = "selected" ';
            }
            $output .='>';
            $output .= '<a href="manage_content.php?page='.urlencode($page["id"]).'"> ';
            $output .= htmlentities($page["menu_name"]);
            $output .= '</a></li>';
        }
        $output .= '</ul>';
        $output .= "</li>";
    }
    //release return data
    mysqli_free_result($subject_set);
    mysqli_free_result($page_set);
    return $output;
}

function find_selected_page(){
    global $current_subject;
    global $current_page;
    if(isset($_GET["subject"])){
        $selected_page_id = null;
        $current_subject = find_subject_by_id($_GET["subject"]);
        $current_page = null;
    }
    elseif(isset($_GET["page"])){
        $selected_subject_id = null;
        $current_page = find_page_by_id($_GET["page"]);
        $current_subject = null;
    }
    else{
        $selected_page_id = null;
        $selected_subject_id = null;
    }
}

//get subject by id
function find_subject_by_id($subject_id){
    global $connection;
    $safe_subject_id = mysqli_real_escape_string($connection,$subject_id);
    //db query
    $query = "SELECT * FROM subjects WHERE id = {$safe_subject_id} LIMIT 1";
    $subject_set = mysqli_query($connection,$query);
    //check for query errors
    confirm_query($subject_set);
    if($subject = mysqli_fetch_assoc($subject_set)) {
        return $subject;
    }
    else{
        return null;
    }
}

//get page by id
function find_page_by_id($page_id){
    global $connection;
    $safe_page_id = mysqli_real_escape_string($connection,$page_id);
    //query on db
    $query = "SELECT * FROM pages WHERE id = {$safe_page_id} LIMIT 1";
    $page_set = mysqli_query($connection,$query);
    confirm_query($page_set);
    if($page = mysqli_fetch_assoc($page_set)){
        return $page;
    }
    else{
        return null;
    }
}