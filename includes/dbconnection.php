<?php
/**
 * Created by PhpStorm.
 * User: Night5talker
 * Date: 15/02/2015
 * Time: 11:32
 */

define("DB_SERVER","localhost");
define("DB_USER","root");
define("DB_PASS","");
define("DB_NAME","widget_corp");

//db connect
$connection = mysqli_connect(DB_SERVER,DB_USER,DB_PASS,DB_NAME);
//test is connection successful
if(mysqli_connect_errno()){
    die ("DB connection failed.");
}