<?php
/**
 * Created by PhpStorm.
 * User: Night5talker
 * Date: 15/02/2015
 * Time: 15:44
 */

session_start();

function message(){
    if(isset($_SESSION["message"])) {
        $output = '<div class="message">';
        $output .= htmlentities($_SESSION["message"]);
        $output .= '</div>';
        $_SESSION["message"] = null;
        return $output;
    }
}


function errors(){
    if(isset($_SESSION["errors"])) {
        $error = $_SESSION["errors"];

        $_SESSION["errors"] = null;
        return $error;
    }
}

