<?php
//included files
require_once("../includes/session.php");
require_once("../includes/dbconnection.php");
require_once("../includes/functions.php");
include_once("../includes/layouts/header.php");

find_selected_page();
?>

<div id="main">
    <div id="navigation">
        <a href="admin.php">&laquo; Main menu</a>

        <?php
            echo navigation($current_subject,$current_page);
        ?>
        <br>

        <a href="new_subject.php">+ Add subject</a>
    </div>
    <div id="page">
        <?php
        echo message();
        ?>
       <?php
       if($current_subject){
           echo "<h2>Manage Subject</h2>";
       }
       elseif($current_page){
           echo "<h2>Manage Page</h2>";
       }
       if($current_subject){
           echo "Menu name:"; echo htmlentities($current_subject["menu_name"]);
           echo '<br><a href="edit_subject.php?subject='.$current_subject["id"].'"">Edit Subject</a>';
           echo "<br>Position: ".$current_subject["position"];
           echo "<br>Visible: ".$current_subject["visible"] == 1 ? 'yes' : 'no';
       }
        elseif($current_page){
            echo "Page name:";
            echo htmlentities($current_page["menu_name"]);
            echo "<br>Position: ".$current_page["position"];
            echo "<br>Visible: ".$current_page["visible"] == 1 ? 'yes' : 'no';

            echo "<br>Content :";
            echo '<br><div class="view-content">';
                echo htmlentities($current_page(["content"]));
            echo '</div>';

        }
        ?>
    </div>
</div>
<?php

include_once("../includes/layouts/footer.php");
?>