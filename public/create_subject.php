<?php
require_once("../includes/session.php");
require_once("../includes/dbconnection.php");
require_once("../includes/functions.php");
require_once("../includes/validation_functions.php");

if(isset($_POST['submit'])){
    $menu_name = mysqli_prep($_POST["menu_name"]);
    $position = (int) $_POST["position"];
    $visible = (int) $_POST["visible"];

    // form validations
    $required_fields = array("menu_name", "position","visible");
    validate_presences($required_fields);

    $fields_with_max_lengths = array("menu_name"=>30);
    validate_max_lengths($fields_with_max_lengths);

    if(!empty($errors)){
        $_SESSION["errors"] = $errors;
        redirect_to("new_subject.php");
    }

    // INSERT query to DB
    $query = "INSERT INTO subjects (menu_name, position, visible)
                VALUES ('{$menu_name}',{$position},{$visible})";
    $result = mysqli_query($connection,$query);

    if($result){
        $_SESSION["message"] = "subject created.";
        redirect_to("manage_content.php");
    }
    else{
        $_SESSION["message"] = "subject creation failed.";
        redirect_to("new_subject.php");
    }
}
else{
    redirect_to("new_subject.php");
}


if(isset($connection)) {
    //close db connecion
    mysqli_close($connection);
}


