<?php
/**
 * Created by PhpStorm.
 * User: Night5talker
 * Date: 22/02/2015
 * Time: 22:04
 */

require_once("../includes/session.php");
require_once("../includes/dbconnection.php");
require_once("../includes/functions.php");


$current_subject = find_subject_by_id($_GET['subject']);

if(!$current_subject){
    redirect_to("manage_content.php");
}

$pages_set = find_pages_for_subjects($current_subject["id"]);

$id = $current_subject["id"];
if(mysqli_num_rows($pages_set) > 0){
    //fail
    $_SESSION["message"] = "cant delete subject with pages";
    redirect_to("manage_content.php?subject={$id}");
}



// repare query for delete
$query = "DELETE FROM subjects WHERE id = {$id} LIMIT 1";

//execute delete on DB
$result = mysqli_query($connection, $query);

if ($result && mysqli_affected_rows($connection) == 1){
    //success
    $_SESSION["message"] = "subject erased";
    redirect_to("manage_content.php");
}
else{
    //fail
    $_SESSION["message"] = "subject deleted";
    redirect_to("manage_content.php?subject={$id}");
}