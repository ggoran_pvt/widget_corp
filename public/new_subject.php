<?php
//included files
require_once("../includes/session.php");
require_once("../includes/dbconnection.php");
require_once("../includes/functions.php");

include_once("../includes/layouts/header.php");


find_selected_page();
?>

<div id="main">
    <div id="navigation">
        <?php
            echo navigation($current_subject,$current_page);
        ?>

    </div>
    <div id="page">
        <?php
        echo message();
        $errors = errors();
        echo form_errors($errors);
        ?>
      <form action="create_subject.php" method="post">
          <p>
              Subject name:
            <input type="text" name="menu_name" value="">
          </p>
          <p>
              Position:
              <select name="position">
                  <?php
                  $subject_set = find_all_subjects();
                  $subject_count = mysqli_num_rows($subject_set);
                  for($count = 1; $count <= $subject_count+1; $count++){
                      echo '<option value="'.$count.'">'.$count.'</option>';
                  }
                  ?>

              </select>
          </p>
          <p>
              Visible:
              <input type="radio" name="visible" value="1"> YES
              <input type="radio" name="visible" value="0"> NO
          </p>
          <input type="submit" name="submit" value="Create Subject">
      </form>
        <br>
        <a href="manage_content.php">Cancel</a>
    </div>
</div>
<?php

include_once("../includes/layouts/footer.php");
?>