<?php
//included files
require_once("../includes/session.php");
require_once("../includes/dbconnection.php");
require_once("../includes/functions.php");
require_once("../includes/validation_functions.php");

include_once("../includes/layouts/header.php");

find_selected_page();

if(!$current_subject){
    redirect_to("manage_content.php");
}

//process if form is submitted
if(isset($_POST['submit'])){

    // form validations
    $required_fields = array("menu_name", "position","visible");
    validate_presences($required_fields);

    $fields_with_max_lengths = array("menu_name"=>30);
    validate_max_lengths($fields_with_max_lengths);

    if(empty($errors)){
        //if no errors perform update
        $id = $current_subject["id"];
        $menu_name = mysqli_prep($_POST["menu_name"]);
        $position = (int) $_POST["position"];
        $visible = (int) $_POST["visible"];

        // INSERT query to DB
        $query  = "UPDATE  subjects SET ";
        $query .= "menu_name = '{$menu_name}', ";
        $query .= "position = '{$position}', ";
        $query .= "visible = '{$visible}' ";
        $query .= "WHERE id = '{$id}' LIMIT 1";

        //submit query to db
        $result = mysqli_query($connection,$query);

        if($result && mysqli_affected_rows($connection) == 1){
            if($result){
                $_SESSION["message"] = "subject updated.";
                redirect_to("manage_content.php");
            }
            else{
                $message = "subject update failed.";
                redirect_to("new_subject.php");
            }
        }
    }
} // end if(isset($_POST['submit']))

?>


    <div id="main">
        <div id="navigation">
            <?php
            echo navigation($current_subject,$current_page);
            ?>

        </div>
        <div id="page">
            <?php
            echo message();
            $errors = errors();
            echo form_errors($errors);
            ?>
            <h2>Edit Subject: <?php echo $current_subject["menu_name"]; ?></h2>
            <form action="edit_subject.php?subject=<?php echo urlencode($current_subject["id"]) ?>" method="post">
                <p>
                    Subject name:
                    <input type="text" name="menu_name" value="<?php echo htmlentities($current_subject["menu_name"]); ?>">
                </p>
                <p>
                    Position:
                    <select name="position">
                        <?php
                        $subject_set = find_all_subjects();
                        $subject_count = mysqli_num_rows($subject_set);
                        for($count = 1; $count <= $subject_count; $count++){
                            echo '<option value="'.$count.'"';
                            if($current_subject["position"] == $count){
                                echo ' selected ';
                            }

                            echo '>'.$count.'</option>';
                        }
                        ?>
                    </select>
                </p>
                <p>
                    Visible:
                    <input type="radio" name="visible" value="1" <?php if($current_subject["visible"]== 1) { echo ' checked ';} ?>> YES
                    <input type="radio" name="visible" value="0" <?php if($current_subject["visible"]== 0) { echo ' checked ';} ?>> NO
                </p>
                <input type="submit" name="submit" value="Edit Subject">
            </form>
            <br>
            <a href="manage_content.php">Cancel</a>
            <a href="delete_subject.php?subject=<?php echo urlencode($current_subject["id"]) ?>">Delete</a>
        </div>
    </div>
<?php

include_once("../includes/layouts/footer.php");
?>