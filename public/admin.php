<?php
require_once("../includes/functions.php");
include_once("../includes/layouts/header.php");
?>
<div id="main">
    <div id="navigation">

    </div>
    <div id="page">
        <h2>Admin Menu</h2>
        <p>Welcome to the admin area.</p>
        <ul>
            <li><a href="manage_content.php">Manage Website content</a> </li>
            <li><a href="manage_admins.php">Manage Admins</a> </li>
            <li><a href="logout.php">Logout</a> </li>
        </ul>
    </div>
</div>
<?php
include_once("../includes/layouts/footer.php");
?>